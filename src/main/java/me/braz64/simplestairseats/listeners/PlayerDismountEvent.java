package me.braz64.simplestairseats.listeners;

import me.braz64.simplestairseats.SimpleStairSeats;
import me.braz64.simplestairseats.utils.SeatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.spigotmc.event.entity.EntityDismountEvent;

public class PlayerDismountEvent implements Listener {

    private final SeatUtils seatUtils;

    public PlayerDismountEvent(SimpleStairSeats plugin) {
        seatUtils = plugin.getSeatUtils();
    }

    @EventHandler
    public void onDismount(EntityDismountEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        Player player = (Player) event.getEntity();

        if (!seatUtils.isPlayerSeated(player)) return;
        seatUtils.unseatPlayer(player);
    }
}
