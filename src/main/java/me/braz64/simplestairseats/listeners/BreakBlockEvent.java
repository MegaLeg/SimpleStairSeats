package me.braz64.simplestairseats.listeners;

import me.braz64.simplestairseats.SimpleStairSeats;
import me.braz64.simplestairseats.utils.SeatUtils;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;
import java.util.Map;

public class BreakBlockEvent implements Listener {
    private final SeatUtils seatUtils;

    public BreakBlockEvent(SimpleStairSeats plugin) {
        seatUtils = plugin.getSeatUtils();
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player playerBreaking = event.getPlayer();
        Player occupyingPlayer;
        Block blockBroken = event.getBlock();

        if (!seatUtils.isBlockOccupied(blockBroken)) return;
        occupyingPlayer = seatUtils.getOccupyingPlayer(blockBroken);
        if (occupyingPlayer == null) return;

        if (occupyingPlayer.equals(playerBreaking)) {
            playerBreaking.sendMessage(ChatColor.RED + "You can't break a block that you're sitting on.");
            event.setCancelled(true);
            return;
        }

        seatUtils.unseatPlayer(occupyingPlayer);
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent event) {
        Block burntBlock = event.getBlock();

        if (seatUtils.isBlockOccupied(burntBlock)) {
            seatUtils.unseatPlayer(seatUtils.getOccupyingPlayer(burntBlock));
        }
    }

    @EventHandler
    public void onBlockExplode(EntityExplodeEvent event) {
        if (event.isCancelled()) {
            return;
        }
        List<Block> blockList = event.blockList();
        Map<Block, Player> seatList = seatUtils.getOccupiedBlocks();

        for (Block currentExplodedBlock : blockList) {
            if (seatList.containsKey(currentExplodedBlock)) {
                Player explodedPlayer = seatList.get(currentExplodedBlock);
                seatUtils.unseatPlayer(explodedPlayer);
            }
        }
    }
}