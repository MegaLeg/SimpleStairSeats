package me.braz64.simplestairseats.listeners;

import me.braz64.simplestairseats.SimpleStairSeats;
import me.braz64.simplestairseats.utils.SeatUtils;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

public class RightClickEvent implements Listener {
    private final SeatUtils seatUtils;

    public RightClickEvent(SimpleStairSeats plugin) {
        this.seatUtils = plugin.getSeatUtils();
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block clickedBlock = event.getClickedBlock();

        if (clickedBlock == null) return;
        if (event.getHand() == null || event.getHand().equals(EquipmentSlot.OFF_HAND)) return;

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
        if (!seatUtils.getSeatBlocks().contains(player.getTargetBlock(null, 2).getType())) return;
        if (!seatUtils.canSeat(player)) return;
        if (!seatUtils.canSeatHere(clickedBlock)) return;

        seatUtils.seatPlayer(player, clickedBlock);
        event.setCancelled(true);
    }
}