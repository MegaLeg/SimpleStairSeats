package me.braz64.simplestairseats.listeners;

import me.braz64.simplestairseats.SimpleStairSeats;
import me.braz64.simplestairseats.utils.SeatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LogoutEvent implements Listener {

    private final SeatUtils seatUtils;

    public LogoutEvent(SimpleStairSeats plugin) {
        this.seatUtils = plugin.getSeatUtils();
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        if (seatUtils.isPlayerSeated(player)) {
            seatUtils.unseatPlayer(player);
        }
    }
}
