package me.braz64.simplestairseats.utils;

import me.braz64.simplestairseats.Seat;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SeatUtils {
    private final Map<Player, Seat> seatedPlayers;
    private final Map<Block, Player> occupiedBlocks;

    public SeatUtils() {
        seatedPlayers = new HashMap<>();
        occupiedBlocks = new HashMap<>();
    }

    public boolean isBlockOccupied(Block block) {
        return occupiedBlocks.getOrDefault(block,null) != null;
    }

    public boolean isPlayerSeated(Player player) {return seatedPlayers.getOrDefault(player, null) != null; }

    @Nullable
    public Player getOccupyingPlayer(Block block) {
        return occupiedBlocks.getOrDefault(block,null);
    }

    public boolean canSeat(Player player) {

        if (player.isSneaking()) {
            return false;
        }

        if (player.isSprinting()) {
            return false;
        }

        if (!player.getInventory().getItemInMainHand().getType().isAir() && player.getInventory().getItemInMainHand().getType().isBlock()) {
            return false;
        }

        if (player.isFlying()) {
            return false;
        }

        return !isPlayerSeated(player);
    }

    public boolean canSeatHere(Block block) {

        //Ensure the block directly above won't cause suffocation
        if (!block.getLocation().add(0,1,0).getBlock().isPassable()) {
             return false;
        }

        //Make sure the stair block is not upside down
        Stairs stairs = (Stairs) block.getBlockData();
        if (stairs.getHalf().equals(Bisected.Half.TOP)) {
            return false;
        }

        //Check to make sure another player isn't sitting there
        return !occupiedBlocks.containsKey(block);
    }

    public Location calculateSeatLocation(Player player, Block block) {

        BlockData blockData = block.getBlockData();
        float playerPitch = player.getLocation().getPitch();
        float locationYaw = 0.0F;
        double locationX = block.getX() + 0.5;
        double locationY = block.getY() + 0.35;
        double locationZ = block.getZ() + 0.5;

        if (blockData instanceof Stairs) {
            Stairs stairs = (Stairs) blockData;
            BlockFace stairsFacing = stairs.getFacing();
            switch(stairsFacing) {
                case EAST:
                    locationYaw = 90.0F;
                    locationX -= 0.1;
                    break;
                case SOUTH:
                    locationYaw = 180.0F;
                    locationZ -= 0.1;
                    break;
                case WEST:
                    locationYaw = -90.0F;
                    locationX += 0.1;
                    break;
                default:
                    locationYaw = 0.0F;
                    locationZ += 0.1;
            }
        }

        return new Location(block.getWorld(), locationX, locationY, locationZ, locationYaw, playerPitch);
    }

    public ArmorStand spawnSeatEntity(Location spawnLocation) {
        if (spawnLocation.getWorld() == null) return null;
        ArmorStand armorStand = spawnLocation.getWorld().spawn(spawnLocation, ArmorStand.class);
        armorStand.setBasePlate(false);
        armorStand.setMarker(true);
        armorStand.setGravity(false);
        armorStand.setAI(false);
        armorStand.setInvulnerable(true);
        armorStand.setInvisible(true);
        armorStand.setCollidable(false);
        armorStand.setRotation(spawnLocation.getYaw(), spawnLocation.getPitch());

        return armorStand;
    }

    public void seatPlayer(Player player, Block seatBlock) {
        if (canSeatHere(seatBlock) && canSeat(player)) {
            Location seatLocation = calculateSeatLocation(player, seatBlock);
            seatedPlayers.put(player, new Seat(seatLocation, spawnSeatEntity(seatLocation), seatBlock));
            occupiedBlocks.put(seatBlock, player);

            Entity seatEntity = seatedPlayers.get(player).getSeatEntity();
            player.teleport(seatEntity);
            seatEntity.addPassenger(player);
            player.playSound(player, Sound.BLOCK_WOOD_STEP, SoundCategory.BLOCKS, 1.0F, 1.0F);
        }
    }

    public void unseatPlayer(Player player) {
        Seat seatToRemove = seatedPlayers.get(player);
        seatToRemove.getSeatEntity().removePassenger(player);
        player.teleport(seatToRemove.getLocation().add(new Vector(0.0,0.65,0.0)));
        player.setSneaking(false);
        seatToRemove.removeEntity();
        seatToRemove.getSeatEntity().remove();
        occupiedBlocks.remove(seatToRemove.getBlock());
        seatedPlayers.remove(player);
    }

    public ArrayList<Material> getSeatBlocks() {
        ArrayList<Material> seatBlocks = new ArrayList<>();
        seatBlocks.add(Material.OAK_STAIRS);
        seatBlocks.add(Material.DARK_OAK_STAIRS);
        seatBlocks.add(Material.BIRCH_STAIRS);
        seatBlocks.add(Material.JUNGLE_STAIRS);
        seatBlocks.add(Material.ACACIA_STAIRS);
        seatBlocks.add(Material.SPRUCE_STAIRS);

        return seatBlocks;
    }

    public Map<Block, Player> getOccupiedBlocks() {
        Map<Block, Player> clonedBlockMap = new HashMap<>();
        for (Block currentBlock : occupiedBlocks.keySet()) {
            clonedBlockMap.put(currentBlock, occupiedBlocks.get(currentBlock));
        }

        return clonedBlockMap;
    }
}