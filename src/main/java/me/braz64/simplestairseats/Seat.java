package me.braz64.simplestairseats;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

public class Seat {
    private final Location location;
    private final Entity seatEntity;
    private final Block block;

    public Seat(Location teleportLocation, Entity seatEntity, Block block) {
        this.location = teleportLocation;
        this.seatEntity = seatEntity;
        this.block = block;
    }

    public Location getLocation() {
        return location;
    }

    public Entity getSeatEntity() {
        return seatEntity;
    }

    public Block getBlock() {
        return block;
    }

    public void removeEntity() {
        this.seatEntity.remove();
    }
}
