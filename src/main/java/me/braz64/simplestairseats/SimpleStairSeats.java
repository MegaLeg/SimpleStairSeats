package me.braz64.simplestairseats;

import me.braz64.simplestairseats.listeners.BreakBlockEvent;
import me.braz64.simplestairseats.listeners.LogoutEvent;
import me.braz64.simplestairseats.listeners.PlayerDismountEvent;
import me.braz64.simplestairseats.listeners.RightClickEvent;
import me.braz64.simplestairseats.utils.SeatUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class SimpleStairSeats extends JavaPlugin {

    private final SeatUtils seatUtils;

    public SimpleStairSeats() {
        seatUtils = new SeatUtils();
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        getServer().getPluginManager().registerEvents(new RightClickEvent(this), this);
        getServer().getPluginManager().registerEvents(new BreakBlockEvent(this), this);
        getServer().getPluginManager().registerEvents(new PlayerDismountEvent(this), this);
        getServer().getPluginManager().registerEvents(new LogoutEvent(this), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        for (Player currentPlayer : Bukkit.getOnlinePlayers()) {
            if (seatUtils.isPlayerSeated(currentPlayer)) {
                seatUtils.unseatPlayer(currentPlayer);
            }
        }
    }

    public SeatUtils getSeatUtils() {
        return seatUtils;
    }

}
