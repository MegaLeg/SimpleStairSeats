# SimpleStairSeats

## Description
Spigot plugin that allows players to right click a stair block and sit down on it.

## Demonstration
Coming Soon...

## Installation
Drop the file "SimpleStairSeats.jar" into your servers plugins folder and restart your server.

## Contributing
If you wish to contribute make a pull request and if the feature is a good idea I will merge it.

## License
There is no licensing on this software. Feel free to use how you please.
